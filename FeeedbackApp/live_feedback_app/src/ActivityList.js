import React from 'react';
import './ActivityList.css';

class ActivityList extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {};
    }
    
    render() {
        let items = this.props.activities.map((activity, index) => 
            <div className="ActivityList" key={index}>
                The activity ID: {activity.activityID} Description: {activity.description} End Time: {activity.endTime} 
            </div>);
        console.log(items);
        return (
            <div>
                {items}
            </div>
        )
    }
}

export default ActivityList;