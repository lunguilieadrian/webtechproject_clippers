import React from 'react';

class AddDateToActivity extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
            date: '',
            hours: '',
            minutes: ''
        };
    }
    
    createEndDate = (event) => {
        this.setState({date: event.target.value}, () => {
            this.onDateAdded();
        });
    }
    
    createEndHours = (event) => {
        this.setState({hours: event.target.value}, () => {
            if(this.state.hours<0||this.state.hours>23){
                window.alert('Invalid hours input');
                this.setState({hours: ''});
            }
            else{
                this.onDateAdded();
            }
        });
    }
    
    createEndMinutes = (event) => {
        this.setState({minutes: event.target.value}, () => {
            if(this.state.minutes<0||this.state.minutes>59){
                window.alert('Invalid minutes input');
                this.setState({minutes: ''});
            }
            else{
                this.onDateAdded();
            }
        });
    }
    
    onDateAdded = () => {
        const newDate = Object.assign({}, this.state);
        this.props.addDate(newDate);
    }
  
    render(){
        return(
            <div>
                <input placeholder="Activity End Time" type="date" value={this.state.date} onChange={this.createEndDate}/>
                <input placeholder="Activity End Hour" type="number" value={this.state.hours} onChange={this.createEndHours}/>
                <input placeholder="Activity End Minute" type="number" value={this.state.minutes} onChange={this.createEndMinutes}/>
             </div>
        );
    }
}

export default AddDateToActivity;