import React, { Component } from 'react';
import './App.css';
import AddReaction from './AddReaction'
import FeedbackList from './FeedbackList'
import ActivityList from './ActivityList'
import CreateActivity from './CreateActivity'
import axios from 'axios';
import student_logo from './media/student_logo.png';
import teacher_logo from './media/teacher_logo.png';
import activities_logo from './media/activities_logo.png';
import feedback_logo from './media/feedback_logo.png';

class App extends Component {
  
  constructor(props) {
        super(props);
        this.state = {
            feedbacks: [],
            activities: []
        };
    }
    
  
  handleStudent = () => {
    this.setState({seeList: false});
    this.setState({seeActivityList: false});
    this.setState({user: 'student'});
  }
  
  handleTeacher = () => {
    this.setState({user: 'teacher'});
  }
  
  componentDidMount = () => {;
    setInterval(() => {
      axios.get('http://18.222.192.25:8080/api/feedback').then(feedbacks => {
      this.setState({
        feedbacks: feedbacks.data
      })
    })
    axios.get('http://18.222.192.25:8080/api/activity').then(activities => {
      this.setState({
        activities: activities.data
      })
    })
    axios.get('http://18.222.192.25:8080/api/feedback').then(activity_code => {
      this.setState({
        activity_id: activity_code.data
      })
    })
    }, 5000)
  }
  
  seeList = () => {
    this.setState({seeList: true});
    this.setState({seeActivityList: false});
  }
  
  seeActivityList = () => {
    this.setState({seeList: false});
    this.setState({seeActivityList: true});
  }
  
  render() {
    return (
      <div className="App">
        <button className = "MainButtons" onClick={this.handleStudent}><img src={student_logo} width="200" height="200" alt="StudentLogo"/></button>
        <button className = "MainButtons" onClick={this.handleTeacher}><img src={teacher_logo} width="200" height="200" alt="TeacherLogo"/></button>
        {this.state.user === "student" && <AddReaction onReactionAdded={this.onReactionAdded}/>}
        {this.state.user === "teacher" === true && 
        <div>
        <CreateActivity onActivityAdded={this.onActivityAdded}/>
        <button className = "ListButtons" onClick={this.seeList}><img src={feedback_logo} width="150" height="150" alt="FeedbackLogo"/></button>
        <button className = "ListButtons" onClick={this.seeActivityList}><img src={activities_logo} width="150" height="150" alt="ActivitiesLogo"/></button>
        </div>}
        {this.state.seeList === true && <FeedbackList feedbacks={this.state.feedbacks}/>}
        {this.state.seeActivityList === true && <ActivityList activities={this.state.activities}/>}
      </div>
    );
  }
}

export default App;