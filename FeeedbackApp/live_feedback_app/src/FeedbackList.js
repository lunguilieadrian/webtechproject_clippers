import React from 'react';
import './FeedbackList.css';

class FeedbackList extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {};
    }
    
    render() {
        let items = this.props.feedbacks.map((feedback, index) => 
        <div className="FeedbackList" key={index}>
            {feedback.reaction} at {feedback.timestamp} on activity {feedback.activityCode}
        </div>);
        console.log(items);
        return (
            <div>
                {items}
            </div>
        )
    }
}

export default FeedbackList;