const express = require('express');
const router = express.Router();

const { createFeedback, getAllFeedbacks, getFeedbacksByActivityCode } = require('./../controller/feedback');
const { createActivity, getAllActivities } = require('./../controller/activity');


router.post('/feedback', createFeedback);
router.get('/feedback', getAllFeedbacks);
router.get('/feedback/activity', getFeedbacksByActivityCode);

router.post('/activity', createActivity);
router.get('/activity', getAllActivities);


module.exports = router;